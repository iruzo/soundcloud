import re
from urllib.request import urlopen


def soundcloud(api, query):
    query = query.replace(" ", "+")
    pattitleandurl = re.compile(
        r'(?<=<li><h2><a href=")(/.+/.+)(?:">)(.+)(?=</a></h2></li>)')
    with urlopen("https://soundcloud.com/search?q=" + query) as result:
        urls, titles = zip(
            *re.findall(pattitleandurl, result.read().decode("utf-8")))
        chosentitle = api.menu("soundcloud", titles)
        if chosentitle != "":
            return chosentitle + " https://soundcloud.com" + urls[titles.index(chosentitle)]
    return ""


def main(api, params):
    return soundcloud(api, params)
